<?php

// Create objects
include("models/m_template.php");
include("models/m_auth.php");

$template = new Template();
$auth = new Auth();

// Set alerts
$template->setAlertTypes(array('success', 'warning', 'error'));


// Start session
session_start();
