<?php

// DB connection settings
$server = 'localhost';
$username = 'teamFriend';
$pass = 'password';
$db = 'oop_login';

// Connect to db
$conn = new mysqli($server, $username, $pass, $db);

// Turn on error reporting
mysqli_report(MYSQLI_REPORT_ERROR);