<?php

include("includes/init.php");

// Log out
$auth->logout();

// Only display the message if you were already logged in.
if($_SESSION['loggedIn']) {
    $template->setAlert("Successfully logged out!");
}

// Redirect
$template->redirect("login.php");