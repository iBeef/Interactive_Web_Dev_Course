<?php

/*
    Temlate Class
    Handles all templating tasks - displaying templates (views), alerts & errors
*/

class Template {

    public $data;
    private $alertTypes;

    // Constructor
    public function __constructor() {

    }

    // Functions
    public function load($url) {
        include($url);
    }

    public function redirect($url) {
        header("Location: $url");
    }
    
    public function setData($name, $value) {
        $this->data[$name] = htmlentities($value, ENT_QUOTES);
    }

    public function getData($name) {
        return $this->data[$name] ?? '';
    }

    // Get/Set Alerts
    public function setAlertTypes($types) {
        $this->alertTypes = $types;
    }

    public function setAlert($value, $type=NULL) {
        $type = $type ?? $this->alertTypes[0];
        $_SESSION[$type][] = $value;
    }

    public function getAlerts() {
        $data = '';
        foreach($this->alertTypes as $alert) {
            if(isset($_SESSION[$alert])) {
                foreach($_SESSION[$alert] as $value) {
                    $data .= "<li class='$alert'>$value</li>";
                }
                unset($_SESSION[$alert]);
            }
        }
        return $data;
    }
}

