<?php

/*
    Authorisation class - Dealwith auth tasks
*/

class Auth {

    private $salt = 'j4H9?s0d';

    // Constructor
    public function __construct() {
    }

    // Functions
    public function validateLogin($user, $pass) {
        // Access db
        global $conn;
        // Create query
        if ($stmt = $conn->prepare("SELECT * FROM users WHERE username = ? AND password = ?")) {
            $hash = md5($pass . $this->salt);
            $stmt->bind_param('ss', $user, $hash);
            $stmt->execute();
            $stmt->store_result();
            // Check for num rows
            if($stmt->num_rows > 0) {
                // Success
                $stmt->free_result();
                $stmt->close();
                return TRUE;
            } else {
                // Success
                $stmt->free_result();
                $stmt->close();
                return FALSE;
            }
        } else {
            die("Error could not prepare MYSQLi statement");
        }
    }

    public function checkLoginStatus() {
        return (isset($_SESSION['loggedIn'])) ? TRUE : FALSE;
    }

    public function logout() {
        session_destroy();
        session_start();
    }
}