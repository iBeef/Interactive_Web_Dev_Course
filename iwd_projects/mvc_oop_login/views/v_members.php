<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Member's Area</title>
    <link rel="stylesheet" href="views/style.css">
</head>
<body>
    <h1>Member's Area</h1>
    <div id="content">
        <?php
            $alerts = $this->getAlerts();
            if($alerts != '') {
                echo "<ul class='alerts'>$alerts</ul>";
            }
        ?>

        <p>You have successfully logged into the member's area.</p>
        <p><a href="logout.php">Logout</a></p>
    </div>
</body>
</html>