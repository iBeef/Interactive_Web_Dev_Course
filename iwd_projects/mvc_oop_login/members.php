<?php

include("includes/init.php");

// Check authorisation
if($auth->checkLoginStatus() == FALSE) {
    // Not authorised
    $template->setAlert("You need to be logged in to view that!", 'error');
    $template->redirect('login.php');
} else {
    // Are authorised
    $template->load("views/v_members.php");
}