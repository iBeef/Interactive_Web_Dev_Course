<?php

// Includes
include("includes/database.php");
include("includes/init.php");

if(isset($_POST['submit'])) {
    // Get data
    $template->setData("inputUsername", $_POST['username']);
    $template->setData("inputPassword", $_POST['password']);

    // Validate the data
    if($_POST['username'] == '' || $_POST['password'] == '') {
        // Show an error message
        if($_POST['username'] == '') {
            $template->setData("errorUsername", "Required field");
        }
        if($_POST['password'] == '') {
            $template->setData("errorPassword", "Required field");
        }
        $template->setalert("Please fill in all required fields.", 'error');
        $template->load("views/v_login.php");
    } elseif ($auth->validateLogin($template->getData("inputUsername"), $template->getData("inputPassword")) == FALSE) {
        // Invalid login
        $template->setAlert("Invalid username or password!", 'error');
        $template->load("views/v_login.php");
    } else {
        $_SESSION['userName'] = $template->getData('inputUsername');
        $_SESSION['loggedIn'] = True;
        $template->setAlert("Welcome <i>" . $template->getData("inputUsername") . "</i>");
        $template->redirect("members.php");
    }

} else {
    $template->load("views/v_login.php");
}

// session_start();

// include("models/m_template.php");

// $template = new Template();

// // $template->setAlert("Success message!");
// // $template->setAlert("Error message!", 'error');
// // $template->setAlert("Warning message!", 'warning');

// // Set data
// $template->setData('name', 'value');

// // Render form
// $template->load("views/v_login.php");