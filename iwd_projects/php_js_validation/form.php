<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Validation Example</title>
    <link rel="stylesheet" href="styles/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/validate.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#form').validate({
                rules: {
                    phone: {
                        required: true,
                        phoneUS: true
                    }
                }
            });
        });
    </script>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" id="form">
        <fieldset>
            <label for="name">Name: <em>*</em></label>
            <input type="text" name="name" id="name" class="required" value="<?php echo $form['name']; ?>"><?php echo $error['name']; ?>

            <label for="name">Phone (000-000-0000): <em>*</em></label>
            <input type="text" name="phone" id="phone" class="required" value="<?php echo $form['phone']; ?>"><?php echo $error['phone']; ?>

            <label for="name">Fax (000-000-0000): </label>
            <input type="text" name="fax" id="fax" value="<?php echo $form['fax']; ?>">

            <label for="name">Email: <em>*</em></label>
            <input type="text" name="email" id="email" class="required email" value="<?php echo $form['email']; ?>"><?php echo $error['email']; ?>

            <label for="name">Comments: </label>
            <textarea name="comments" id="comments"><?php echo $form['comments']; ?></textarea>

            <p class="required_msg">* required fields</p>

            <input type="submit" name="submit" id="submit">

        </fieldset>
    </form>
</body>
</html>