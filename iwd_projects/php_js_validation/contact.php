<?php

    // Email Variables
    $to = "example@example.com";
    $subject = "Message from your website";

    // Variables
    $errorOpen = "<label class='error'>";
    $errorClose = "</label>";
    $validForm = TRUE;
    $redirect = "success.php";

    $formElements = array('name', 'phone', 'fax', 'email', 'comments');
    $required = array('name', 'phone', 'email');

    foreach($required as $require) {
        $error[$require] = '';
    }

    if(isset($_POST['submit'])) {
        // Process form

        // Get form data
        foreach($formElements as $element) {
            $form[$element] = htmlspecialchars($_POST[$element]);
        }

        // Check form elements

        // Check required fields
        if($form['name'] == '') {
            $error['name'] = $errorOpen . "Please fill in all required fields." . $errorClose;
            $validForm = FALSE;
        }
        if($form['phone'] == '') {
            $error['phone'] = $errorOpen . "Please fill in all required fields." . $errorClose;
            $validForm = FALSE;
        }
        if($form['email']  == '') {
            $error['email'] = $errorOpen . "Please fill in all required fields." . $errorClose;
            $validForm = FALSE;
        }

        // Check formatting
        if($error['phone'] == '' && !preg_match("/^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$/", $form['phone'])){
            $error['phone'] = $errorOpen . "Please enter a valid phone number." . $errorClose;
            $validForm = FALSE;
        }
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        if($error['email'] == '' && !preg_match($pattern, $form['email'])){
            $error['email'] = $errorOpen . "Please enter a valid email." . $errorClose;
            $validForm = FALSE;
        }

        // Check for bad data
        if(containsBadStr($form['name']) ||
           containsBadStr($form['email']) ||
           containsBadStr($form['phone']) ||
           containsBadStr($form['fax']) ||
           containsBadStr($form['comments'])) {
               $validForm = FALSE;
            }

        if(containsNewLines($form['name']) ||
            containsNewLines($form['email']) ||
            containsNewLines($form['phone']) ||
            containsNewLines($form['fax'])) {
               $validForm = FALSE;
            }

        // Check if form is still valid
        if($validForm) {
            // Create email message
            $message = "Name: {$form['name']}\n";
            $message .= "Email: {$form['email']}\n";
            $message .= "Phone: {$form['phone']}\n";
            $message .= "Fax: {$form['fax']}\n\n";
            $message .= "Message: {$form['comments']}\n";

            // Headers
            $headers = "From: test@test.com\r\n";
            $headers .= "X-Sender: <test@test.com>\r\n";
            $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
            $headers .= "Reply-To: {$form['email']}\r\n";

            // Send the email
            mail($to, $subject, $message, $headers);

            // redirect
            header("Location: $redirect");
        } else {
            include("form.php");    
        }

    } else {
        foreach($formElements as $element) {
            $form[$element] = '';
        }
        // Display form
        include("form.php");
    }

    function containsBadStr($testStr) {
        $badStrings = array(
            "content-type:",
            "mime-version:",
            "multipart/mixed",
            "content-transfer-encoding:",
            "bcc",
            "cc",
            "to"
        );
        foreach($badStrings as $badString) {
            if(stristr(strtolower($testStr), $badString)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function containsNewLines($testStr) {
        if(preg_match("/(%0A|%0D|\\n+|\\r+)/i", $testStr) != 0) {
            return TRUE;
        }
        return FALSE;
    }

?>