<?php

class TagCloudModel{
    public $conn;
    public $tags; // array(id, name, total)
    public $largest;

    function __construct($conn) {
        $this->conn = $conn;

        // Get tags and largest value
        $this->getTags();
        // echo "<pre>";
        //     print_r($this->tags);
        // echo "</pre>";
    }

    // Utility Functions
    function getTags() {
        $this->tags = array();
        $this->largest = 0;

        $result = $this->conn->query("SELECT posts_to_tags.tag_id, tags.name,
            COUNT(posts_to_tags.tag_id) AS total 
            FROM tags, posts_to_tags
            WHERE posts_to_tags.tag_id = tags.id
            GROUP BY posts_to_tags.tag_id");
        if($result->num_rows > 0) {
            while($row = $result->fetch_object()) {
                // Check for largest
                $this->largest = ($row->total > $this->largest) ? $row->total : $this->largest;

                // Add info to array
                $this->tags[] = array('id' => $row->tag_id, 'name' => $row->name, 'total' => $row->total);

                // Sort tags
                usort($this->tags, array("TagCloudModel", "compareNames"));
            }
        } else {
            $this->tags = FALSE;
        }
    }
    
    // Display Functions
    function getTagList() {
        if($this->tags) {
            $data = '';
            $data .= "<table border='1' cellpadding='10'>";
            $data .= "<tr><th>ID</th><th>Tag Name</th><th>Count</th><th>Weight</th></tr>";
            foreach($this->tags as $tag) {
                // Find the weight
                $weight = round(($tag['total'] / $this->largest) * 10);
                $data .= "<tr>";
                $data .= "<td>{$tag['id']}</td>";
                $data .= "<td>{$tag['name']}</td>";
                $data .= "<td>{$tag['total']}</td>";
                $data .= "<td>$weight</td>";
                $data .= "</tr>";
            }
            $data .= "</table>";
            return $data;
            
        } else {
            return "No tags to display";
        }
    }

    function getTagCloud() {
        if($this->tags) {
            // Create unordered list
            $data = '';
            $data .= "<ul class='tagCloud'>";
            foreach($this->tags as $tag) {
                // Find weight
                $weight = round(($tag['total'] / $this->largest) * 10);
                $data .= "<li><a href='tags.php?=".$tag['id']."' class='tag$weight'>{$tag['name']}</a></li>";
                $data .= "";
            }
            $data .= "</ul>";
            return $data;
        } else {
            return "No tags to display";
        }
    }

    function compareNames($a, $b) {
        return strcmp($a['name'], $b['name']);
    }

}

?>