<?php

// Open db connection
include_once("models/open_db.php");

// Load models
include_once("models/tag_cloud.model.php");

// Create tag cloud
$tagCloud = new TagCloudModel($conn);
$data["tagList"] = $tagCloud->getTagList();
$data["tagCloud"] = $tagCloud->getTagCloud();

// Load view
include("views/tag_cloud.view.php");

// Close db connection
include_once("models/close_db.php");

?>