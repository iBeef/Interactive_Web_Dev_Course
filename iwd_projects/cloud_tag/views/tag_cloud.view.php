<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tag Cloud</title>
    <style>
        table: {width: 340px; border-collapse: collapse;}
        .tagCloud {margin: 0; padding: 20px; width: 300px; border: 1px solid black;
            text-align: center; line-height: 2.8em; }
        .tagCloud li {display: inline-block; padding: 0 5px;}
        .tagCloud a {padding: 0;}
        .tagCloud a.tag1 {font-size: 0.7em; font-weight:100;}
        .tagCloud a.tag2 {font-size: 0.8em; font-weight:200;}
        .tagCloud a.tag3 {font-size: 0.9em; font-weight:300;}
        .tagCloud a.tag4 {font-size: 1em; font-weight:400;}
        .tagCloud a.tag5 {font-size: 1.2em; font-weight:500;}
        .tagCloud a.tag6 {font-size: 1.4em; font-weight:600;}
        .tagCloud a.tag7 {font-size: 1.6em; font-weight:700;}
        .tagCloud a.tag8 {font-size: 1.8em; font-weight:800;}
        .tagCloud a.tag9 {font-size: 2.2em; font-weight:900;}
        .tagCloud a.tag10 {font-size: 2.5em; font-weight:900;}
    </style>
</head>
<body>
    <h1>Tag CLoud</h1>

    <h2>List</h2>
    <?php echo $data["tagList"]; ?>

    <h2>Cloud</h2>
    <?php echo $data["tagCloud"]; ?>

</body>
</html>