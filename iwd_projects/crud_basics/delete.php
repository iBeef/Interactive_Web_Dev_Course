<?php

include("connectDb.php");
if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    // Set id variable and make sure it's ok for mysql
    $id = mysqli_real_escape_string($conn, $_GET['id']);
    // Prepare safe way of querying database
    if($stmt = $conn->prepare("DELETE FROM players WHERE id = ? LIMIT 1")) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
    } else {
        echo "Error: Could not prepare SQL staement.";
    }
    $conn->close();
    header("Location: view.php");
} else {
    header("Location: view.php");
}

?>