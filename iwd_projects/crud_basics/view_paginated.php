<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View Records</title>
</head>
<body>
    <h1>View Records</h1>
    <?php
        include("connectDb.php");
        
        $perPage = 3;

        if($result = $conn->query("SELECT * FROM players ORDER BY id")) {
            if($result->num_rows > 0) {
                $totalResults = $result->num_rows;
                $totalPages = ceil($totalResults / $perPage);

                if(isset($_GET['page']) && is_numeric($_GET['page'])) {
                    $showPage = $_GET['page'];
                    if($showPage > 0 && $showPage <= $totalPages) {
                        $start = ($showPage - 1) * $perPage;
                        $end = $start + ($perPage - 1);
                    } else {
                        $start = 0;
                        $end = $perPage;
                    }
                } else {
                    $start = 0;
                    $end = $perPage;
                }
            } else {
                echo "No results to display";
            }

            // Display pagination
            echo "<p><a href='view.php'>View All</a> | <b>View Page: </b>";
            for($i = 1; $i <= $totalPages; $i++) {
                if(isset($_GET['page']) && $_GET['page'] == $i) {
                    echo "$i ";
                } else {
                    echo "<a href='view_paginated.php?page=$i'>$i</a> ";
                }
            }
            echo "</p>";
            // Display Records
            echo "<table border='1' cellpadding='10'>";
            echo "<tr><th>Id</th><th>First Name</th><th>Last Name</th><th></th><th></th></tr>";
            for($i = $start; $i <= $end; $i++) {
                if($i == $totalResults) {
                    break;
                }
                $result->data_seek($i);
                $row = $result->fetch_row();
                // print_r($row);
                echo "<tr>";
                echo "<td>{$row[0]}</td>";
                echo "<td>{$row[1]}</td>";
                echo "<td>{$row[2]}</td>";
                echo "<td><a href='records.php?id=" . $row[0] . "'>Edit</td>";
                echo "<td><a href='delete.php?id=" . $row[0] . "'>Delete</td>";
                echo "</tr>";
            }
            echo "</table>";

        } else {
            echo "Error: {$conn->error}";
        }

        $conn->close();
    ?>
    <a href="records.php">Add New Record</a>
</body>
</html>