<?php
include("connectDb.php");

function renderForm($first='', $last='', $error='', $id='') {

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?php echo ($id === '') ? "New Record" : "Edit Record"; ?></title>
    </head>
    <body>
        <h1><?php echo ($id === '') ? "New Record" : "Edit Record"; ?></h1>
        <?php
            if($error != '') {
                echo "<div style='padding: 4px; border: 1px solid red; color: red;'>$error</div>";
            }
        ?>
        <form action="<?php echo isset($_GET['id']) ? $_SERVER['PHP_SELF'] . "?id={$_GET['id']}" : $_SERVER['PHP_SELF']; ?>" method="POST">
        <!-- <form action="" method="POST"> -->
            <div>
            <?php if($id != '') : ?>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <p>Id: <?php echo $id; ?></p>
            <?php endif; ?>
            <strong>First Name: *</strong>
            <input type="text" name="firstName" value="<?php echo $first; ?>">
            <br>
            <strong>Last Name: *</strong>
            <input type="text" name="lastName" value="<?php echo $last; ?>">
            <p>* required</p>
            <input type="submit" name="submit" value="Submit">
            </div>
        </form>
    </body>
    </html>
<?php
}

if(isset($_GET['id'])) {
    if(isset($_POST['submit'])) {
        if(is_numeric($_GET['id']) && $_GET['id'] > 0) {
            $id = mysqli_real_escape_string($conn, $_POST['id']);
            $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
            $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);

            if($firstName === '' || $lastName === '') {
            $error = "Error: Please fill in all required fields.";
            renderForm($firstName, $lastName, $error);
            } else {
                if($stmt = $conn->prepare("UPDATE players SET firstName = ?, lastName= ? WHERE id = ?")) {
                    $stmt->bind_param("ssi", $firstName, $lastName, $id);
                    $stmt->execute();
                    $stmt->close();
                } else {
                    echo "Error: Could not prepare the sql statement.";
                }
                header("Location: view.php");
            }
        } else {
            echo "Error!";
        }
    } else {
        if(is_numeric($_GET['id']) && $_GET['id'] > 0) {
            // Query database
            $id = mysqli_real_escape_string($conn, $_GET['id']);
            if($stmt = $conn->prepare("SELECT * FROM players WHERE id = ?")) {
                $stmt->bind_param('i', $id);
                $stmt->execute();
                $stmt->bind_result($id, $firstName, $lastName);
                $stmt->fetch();
                $stmt->close();
                renderForm($firstName, $lastName, NULL, $id);
            } else {
                echo "Could not prepare SQL Statement.";
            }
        } else {
            header("Location: view.php");
        }
    }
} else {
    if(isset($_POST['submit'])) {
        $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
        // $firstName = htmlentities($_POST['firstName'], ENT_QUOTES);
        $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
        // $lastName = htmlentities($_POST['lastName'], ENT_QUOTES);

        if($firstName === '' || $lastName === '') {
            $error = "Error: Please fill in all required fields.";
            renderForm($firstName, $lastName, $error);
        } else {
            if($stmt = $conn->prepare("INSERT INTO players (firstName, lastName) VALUES (?, ?)")) {
                $stmt->bind_param("ss", $firstName, $lastName);
                $stmt->execute();
                $stmt->close();
            } else {
                echo "Error: Could not prepare the sql statement.";
            }
            header("Location: view.php");
        }
    } else {
        renderForm();
    }
}
$conn->close();
?>