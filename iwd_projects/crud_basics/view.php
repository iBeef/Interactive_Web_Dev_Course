<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View Records</title>
</head>
<body>
    <h1>View Records</h1>
    <p><b>View All</b> | <a href="view_paginated.php">View Paginated</a>
    <?php
        include("connectDb.php");
        if($result = $conn->query("SELECT * FROM players ORDER BY id")) {
            if($result->num_rows > 0) {
                echo "<table border='3' cellpadding='10'>";
                echo "<tr><th>Id</th><th>First Name</th><th>Last Name</th><th></th><th></th></tr>";
                while($row = $result->fetch_object()) {
                    echo "<tr>";
                    echo "<td>{$row->id}</td>";
                    echo "<td>{$row->firstName}</td>";
                    echo "<td>{$row->lastName}</td>";
                    echo "<td><a href='records.php?id=" . $row->id . "'>Edit</td>";
                    echo "<td><a href='delete.php?id=" . $row->id . "'>Delete</td>";
                    echo "</tr>";
                }
                echo "</table>";
            } else {
                echo "No results to display.";
            }
        } else {
            echo "Error: " . $conn->error;
        }
        $conn->close();
    ?>
    <a href="records.php">Add New Record</a>
</body>
</html>