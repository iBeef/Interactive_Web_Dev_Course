<?php

include("app/init.php");

$template->setData("pageClass", 'home');

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    // Get products from specific category
    $category = $categories->getCategories($_GET['id']);
    if( ! empty($category)) {
        // Get category nav
        // Get category navigation
        $categoryNav = $categories->createCategoryNav($category['name']);
        $template->setData("pageNav", $categoryNav);
        // get all products from that category
        $catProducts = $products->createProductTable(4, $_GET['id']);
        if( ! empty($catProducts)) {
            $template->setData('products', $catProducts);
        } else {
            $template->setData('products', "<li>No products exist in this category</li>");
        }
        $template->load("app/views/v_public_home.php", $category['name']);
    } else {
        // Category not valid
        $template->redirect(SITE_PATH);
    }
} else {
    // Get all products
    $productsList = $products->createProductTable();
    $template->setData('products', $productsList);

    // Get category navigation
    $categoryNav = $categories->createCategoryNav('home');
    $template->setData("pageNav", $categoryNav);
    $template->load("app/views/v_public_home.php", 'Welcome!');
}

// $productsList = $products->getInCategory(2);
// echo "<pre>";
// print_r($productsList);
// echo "</pre>";
// exit;
