<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html">
    <link rel="stylesheet" href="resources/css/style.css">
    <title><?php $this->getData("pageTitle") ?></title>
</head>
<body class="<?php $this->getData('pageClass') ?>">
    <div id="wrapper">
        <div class="secondaryNav">
            <strong><?php $this->getData("cartTotalItems") ?> 
            <?php echo ($this->getData("cartTotalItems", FALSE) == 1) ? "item" : "items" ?> 
            <?php
                $price = $this->getData("cartTotalCost", FALSE);
                echo ($price == 0) ? "(£0.00)" : "(£$price)";
            ?> 
            in cart</strong> &nbsp; | &nbsp;
            <a href="<?php echo SITE_PATH; ?>cart.php">Shopping Cart</a>
        </div>
        <h1><?php echo SITE_NAME; ?></h1>
        <ul class="nav">
            <?php $this->getData("pageNav"); ?>
        </ul>