<?php include("includes/public_header.php"); ?>

<div id="content">
    <h2>Shopping Cart</h2>

    <ul class="alerts">
        <?php $this->getAlerts(); ?>
    </ul>

    <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
        <ul class="cart">
            <?php $this->getData('cartRows'); ?>
        </ul>
        <div class="buttonsRow">
            <a class="buttonAlt" href="?empty">Empty Cart</a>
            <input type="submit" name="update" value="Update Cart" class="buttonAlt">
        </div>
    </form>

    <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
        <div class="submitRow">
            <input type="submit" name="submit" value="Pay with PayPal" class="button">
        </div>
    </form>

</div>

<?php include("includes/public_footer.php"); ?>