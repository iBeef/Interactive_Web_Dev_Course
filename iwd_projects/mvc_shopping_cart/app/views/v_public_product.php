<?php include("includes/public_header.php"); ?>

<div id="content">

        <img src="<?php $this->getData('prodImage'); ?>" alt="<?php $this->getData('prodName'); ?>" class="productImage">
        <h2><?php $this->getData('prodName'); ?></h2>
        <div class="price">£<?php $this->getData('prodPrice'); ?></div>
        <div class="description"><?php $this->getData('prodDescription'); ?></div>
        <a href="cart.php?id=<?php $this->getData('prodId'); ?>" class="button">Add to Cart</a>

</div>

<?php include("includes/public_footer.php"); ?>