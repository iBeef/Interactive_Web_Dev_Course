<?php

/*
    INIT - Basic config settings
*/

// Connect to db
$server = "localhost";
$user = "teamFriend";
$pass = "password";
$db = "ks_shop";

$conn = new mysqli($server, $user, $pass, $db);

// Error_reporting
mysqli_report(MYSQLI_REPORT_ERROR);
ini_set("display_errors", '1');

// Set up constants
define("SITE_NAME", "My Online Store");
define("SITE_PATH", "http://localhost/mvc_shopping_cart/");
define("IMAGE_PATH", "http://localhost/mvc_shopping_cart/resources/images/");
define("SHOP_TAX", '0.2');
 
// Include objects
include("app/models/m_template.php");
include("app/models/m_categories.php");
include("app/models/m_products.php");
include("app/models/m_cart.php");

// Define models 
$template = new Template();
$categories = new Categories();
$products = new Products();
$cart = new Cart();

// Start a session
session_start();

// Global
$template->setData("cartTotalItems", $cart->getTotalItems());
$template->setData("cartTotalCost", $cart->getTotalCost());