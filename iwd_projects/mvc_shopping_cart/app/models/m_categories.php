<?php

/*
    Categories class
    Handles all tasks related to retrieving and displaying categories.
*/

class Categories {

    private $db;
    private $db_table = 'categories';

    public function __construct() {
        global $conn;
        $this->db = $conn;
    }

    // Setting/Getting categories from the database
    

    /**
     * Return an array with category information
     *
     * @access public
     * @param int $id (optional)
     * @return array
     */
    public function getCategories($id=NULL) {
        $data = array();
        if($id != NULL) {
            // Get specific category
            if($stmt = $this->db->prepare("SELECT id, name FROM " . $this->db_table . " WHERE id = ? LIMIT 1")) {
                $stmt->bind_param('i', $id);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($catId, $catName);
                $stmt->fetch();
                if($stmt->num_rows > 0) {
                    $data = array('id' => $catId, 'name' => $catName);
                }
                $stmt->close();
            }
        } else {
            // Get all categories
            if($result = $this->db->query("SELECT * FROM " . $this->db_table . " ORDER BY name")) {
                if($result->num_rows > 0) {
                    while($row = $result->fetch_array()) {
                        $data[] = array('id' => $row['id'], 'name' => $row['name']);
                    }
                }
            }
        }
        return $data;
    }

    // Create page parts

    /**
     * Returns an unordered list of links to all category pages
     *
     * @access public
     * @param string $active (optional)
     * @return string
     */
    public function createCategoryNav($active=NULL) {
        // Get all categories
        $categories = $this->getCategories();

        // Setup 'all' item
        $data = "<li";
        if($active == strtolower('home')) {
            $data .= ' class="active"';
        }
        $data .= '><a href="' . SITE_PATH . '">View All</a></li>';
        // Loop through each category
        if(! empty($categories)) {
            foreach($categories as $category) {
                $data .= "<li";
                if(strtolower($active) == strtolower($category['name'])) {
                    $data .= ' class="active"';
                }
                $data .= '><a href="' . SITE_PATH . 'index.php?id=' . $category['id'] . '">' . $category['name'] . "</a></li>";
            }
        }
        return $data;
    }
}