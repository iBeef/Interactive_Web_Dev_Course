<?php

/*
    Template Class
    Handles all templating tasks, displaying:
        - Views
        - Alerts
        - Errors
        - View data
*/

class Template {

    private $data;
    private $alertTypes = array('success', 'alert', 'error');

    public function __construst() {}
    
    /**
     * Loads specified url
     *
     * @access public
     * @param string $url
     * @param string $title
     * @return NULL
     */
    public function load($url, $title=NULL) {
        if($title != '') {
            $this->setData("pageTitle", $title);
        }
        include($url);
    }

    /**
     * Redirects to specified url
     *
     * @access public
     * @param string $url
     * @return NULL
     */
    public function redirect($url) {
        header("Location: $url");
        exit;
    }

    /*
        Get/Set data
    */

    /**
     * Saves provided data for use by the view later
     *
     * 
     * @access public
     * @param string $name
     * @param string $value
     * @param boolean $clean
     * @return NULL
     */
    public function setData($name, $value, $clean=FALSE) {
        if($clean == TRUE) {
            $this->data[$name] = htmlentities($value, ENT_QUOTES);
        } else {
            $this->data[$name] = $value;
        }
    }

    /**
     * Retrieves data on provided name for access by view
     *
     * @access public
     * @param string $name
     * @param boolean $echo
     * @return string
     */
    public function getData($name, $echo=TRUE) {
        if(isset($this->data[$name])) {
            if($echo) {
                echo $this->data[$name];
            } else {
                return $this->data[$name];
            }
        }
        return '';
    }

    /*
        Get/Set alerts
    */

    /**
     * Sets an alert message stored in a session variable
     *
     * @access public
     * @param string $value
     * @param string $type (optional)
     * @return NULL
     */
    public function setAlert($value, $type='success') {
        $_SESSION[$type][] = $value;
    }

    /**
     * Returns string, containing multiple list items of alerts
     *
     * @access public
     * @return string
     */
    public function getAlerts() {
        $data = '';
        
        foreach($this->alertTypes as $alert) {
            if(isset($_SESSION[$alert])) {
                foreach($_SESSION[$alert] as $value) {
                    $data .= "<li class='" . $alert . "'>" . $value . "</li>";
                }
                unset($_SESSION[$alert]);
            }
        }
        echo $data;
    }
}