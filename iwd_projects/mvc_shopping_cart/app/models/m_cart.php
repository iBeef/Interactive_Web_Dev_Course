<?php

/*
    Cart Class
    Handles all tasks related to showing or modifying the number of items in the cart.

    The cart keeps track of user selected items using a session variable $_SESSION['cart'].
    The session variable contains an array of ids nd the number of selected products in the cart.

    $_SESSION['cart'][productId] = Number of specific item in the cart

*/

class Cart {
    
    public function __construct() {
    }

    /**
     * Return an array of all product info for items in the cart
     *
     * @access public
     * @return array, NULL
     */
    public function get() {
        if(isset($_SESSION['cart'])) {
            // Get all the products ids of items in the cart
            $ids = $this->getIds();
            // Use list of ids to get product info
            global $products;
            return $products->get($ids);
        }
        return NULL;
    }

    /**
     * Return an array of all product ids in cart
     *
     * @access public
     * @return array, NULL
     */
    public function getIds() {
        if(isset($_SESSION['cart'])) {
            return array_keys($_SESSION['cart']);
        }
        return NULL;
    }

    /**
    * Adds item to the cart
    *
    * @access public
    * @param int $id
    * @param int $quantity
    * @return NULL
    */
    public function add($id, $quantity=1) {
        // Set up or retrieve the cart
        $cart = $_SESSION['cart'] ?? array();
        // Check to see if item is already in the cart
        $_SESSION['cart'][$id] = (isset($_SESSION['cart'][$id])) ? $_SESSION['cart'][$id] + $quantity : $_SESSION['cart'][$id] = 1; 
    }

    /**
     * Update the quantity of a specific item in the cart
     *
     * @access public
     * @param int $id
     * @param int $quantity
     * @return NULL
     */
    public function update($id, $quantity) {
        if($quantity == 0) {
            unset($_SESSION['cart'][$id]);
            if(empty($_SESSION['cart'])) {
                unset($_SESSION['cart']);
            }
        } else {
            $_SESSION['cart'][$id] = $quantity;
        }
    }

    /**
     * Empties all items from the cart.
     * @access public
     * @return NULL
     */
    public function emptyCart() {
        unset($_SESSION['cart']);
    }

    /**
     * Return total number of items in the cart
     *
     * @access public
     * @return int
     */
    public function getTotalItems() {
        $num = 0;
        if(isset($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $numItems) {
                $num += $numItems;
            }
        }
        return $num;
    }

    /**
     * Return total cost for all items in the cart
     *
     * @access public
     * @return int
     */
    public function getTotalCost() {
        $num = 0.00;
        if(isset($_SESSION['cart'])) {
            // If items to display
            $ids = $this->getIds();
            // Get product prices
            global $products;
            $prices = $products->getPrices($ids);

            // Loop through adding the cost of each item x quantity to $num
            if($prices != NULL) {
                foreach ($prices as $price) {
                    $num += doubleval($price['price'] * $_SESSION['cart'][$price['id']]);
                }
            }
        }
        return $num;
    }

    /**
     * Return shipping cost based on cost of items.
     *
     * @param double $total
     * @return double
     */
    public function getShippingCost($total) {
        if($total > 200) {
            return 40.00;
        } else if($total > 50) {
            return 15.0;
        } else if($total > 10) {
            return 4.0;
        } else {
            return 2.0;
        }
    }

    /*
        Create Page Parts
    */

    /**
     * Return a string containing list items for each product in the cart.
     *
     * @access public
     * @return string
     */
    public function createCart() {
        // Get products currently in cart
        $products = $this->get();
        $data = '';
        $total = 0;
        $data .= '<li class="headerRow">';
        $data .= '<div class="col1">Product Name:</div>';
        $data .= '<div class="col2">Quantity:</div>';
        $data .= '<div class="col3">Product Price:</div>';
        $data .= '<div class="col4">Total Price:</div>';
        $data .= "</li>";
        if($products != "") {
            // Products to display
            $line = 1;
            $shipping = 0;
            foreach ($products as $product) {
                // Create a new item in cart
                $data .= '<li';
                if($line % 2 == 0) {
                    $data .= ' class="alt"';
                }
                $data .= '>';
                $data .= '<div class="col1">' . $product['name'] . '</div>';
                $data .= '<div class="col2"><input name="product' . $product['id'] . '" value="' . $_SESSION['cart'][$product['id']] . '"></div>';
                $data .= '<div class="col3">£' . $product['price'] . '</div>';
                $data .= '<div class="col4">£' . $product['price'] * $_SESSION['cart'][$product['id']] . '</div>';
                $shipping += $this->getShippingCost($product['price'] * $_SESSION['cart'][$product['id']]);
                $total += $product['price'] * $_SESSION['cart'][$product['id']];
                $line++;
            }
            // Add subtotal row
            $data .= '<li class="subtotalRow">';
            $data .= '<div class="col1">Subtotal:</div>';
            $data .= '<div class="col2">£' . $total . '</div>';
            $data .= "</li>";
            // Add shipping row
            $data .= '<li class="shippingRow">';
            $data .= '<div class="col1">Shipping Cost:</div>';
            $data .= '<div class="col2">£' . number_format($shipping, 2) . '</div>';
            $data .= "</li>";
            // Add  taxes row
            if(SHOP_TAX > 0) {
                $data .= '<li class="taxesRow">';
                $data .= '<div class="col1">Tax (' . (SHOP_TAX * 100) . '%):</div>';
                $data .= '<div class="col2">£' . number_format(SHOP_TAX * $total, 2) . '</div>';
                $data .= '</li>';
            }
            // Add a total row
            $data .= '<li class="totalRow">';
            $data .= '<div class="col1">Total:</div>';
            $data .= '<div class="col2">£' . $total . '</div>';
            $data .= "</li>";
        } else {
            // No products to display
            $data .= "<li><strong>No items in the cart!</strong></li>";
            // Add subtotal row
            $data .= '<li class="subtotalRow">';
            $data .= '<div class="col1">Subtotal:</div>';
            $data .= '<div class="col2">£0.00</div>';
            $data .= "</li>";
            // Add shipping row
            $data .= '<li class="shippingRow">';
            $data .= '<div class="col1">Shipping Cost:</div>';
            $data .= '<div class="col2">£0.00</div>';
            $data .= "</li>";
            // Add  taxes row
            if(SHOP_TAX > 0) {
                $data .= '<li class="taxesRow">';
                $data .= '<div class="col1">Tax (' . (SHOP_TAX * 100) . '%):</div>';
                $data .= '<div class="col2">£0.00</div>';
                $data .= '</li>';
            }
            // Add a total row
            $data .= '<li class="totalRow">';
            $data .= '<div class="col1">Total:</div>';
            $data .= '<div class="col2">£0.00</div>';
            $data .= "</li>";
        }
        return $data;
    }
}
