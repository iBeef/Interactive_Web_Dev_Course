<?php

/*
    Products Class
    Handles all tasks reated to retrieving and diplaying products
*/

class Products {

    private $db;
    private $dbTable = 'products';

    public function __construct() {
        global $conn;
        $this->db = $conn;
    }

    // Getters and Setters

    /**
     * Retrieves product information from the database
     *
     * @access public
     * @param integer $id (optional)
     * @return array
     */
    public function get($id=NULL) {
        $data = array();
        if(is_array($id)) {
            // Get products based on an array of ids
            $items = '';
            foreach($id as $item) {
                $items .= ($items == '') ? $item : "," . $item;
            }
            if($result = $this->db->query("SELECT id, name, description, price, image FROM $this->dbTable WHERE id IN ($items) ORDER BY name")) {
                if($result->num_rows > 0) {
                    while($row = $result->fetch_array()) {
                        $data[] = array(
                            'id' => $row['id'],
                            'name' => $row['name'],
                            'description' => $row['description'],
                            'price' => $row['price'],
                            'image' => $row['image'],
                        );
                    }
                }
            }
        } else if ($id != NULL) {
            // Get one specific product
            if($stmt = $this->db->prepare("SELECT
            $this->dbTable.id,
            $this->dbTable.name,
            $this->dbTable.description,
            $this->dbTable.price,
            $this->dbTable.image,
            categories.name AS category_name
            FROM $this->dbTable, categories
            WHERE $this->dbTable.id = ?
            AND $this->dbTable.categoryId = categories.id")) {
                $stmt->bind_param('i', $id);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($prodId, $prodName, $prodDescription, $prodPrice, $prodImage, $catName);
                $stmt->fetch();

                if($stmt->num_rows > 0) {
                    $data = array(
                        'id' => $prodId, 'name' => $prodName, 'description' => $prodDescription,
                        'price' => $prodPrice, 'image' => $prodImage, 'categoryName' => $catName
                        );
                    $stmt->close();
                }
            }
        } else {
            // Get all products
            if($result = $this->db->query("SELECT * FROM " . $this->dbTable ." ORDER BY name")) {
                if($result->num_rows > 0) {
                    while($row = $result->fetch_array()) {
                        $data[] = array(
                            'id' => $row['id'],
                            'name' => $row['name'],
                            'price' => $row['price'],
                            'image' => $row['image'],
                        );
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Retrieve product information for all products in a specific category.
     *
     * @access public
     * @param integer $id (optional) 
     * @return string
     */
    public function getInCategory($id) {
        $data = array();
        if($stmt = $this->db->prepare("SELECT id, name, price, image FROM " . $this->dbTable . " WHERE categoryId = ? ORDER BY name")) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($prodId, $prodName, $prodPrice, $prodImage);
            while($stmt->fetch()) {
                $data[] = array(
                    'id' => $prodId,
                    'name' => $prodName,
                    'price' => $prodPrice,
                    'image' => $prodImage
                );
            }
            $stmt->close();
        return $data;
        }
    }

    /**
     * Return an array of price info for each specified id
     *
     * @param array $ids
     * @return array
     */
    public function getPrices($ids) {
        $data = array();
        // Create a comma seperated list based on array of ids.
        $items = '';
        foreach($ids as $item) {
            $items .= ($items == '') ? $item : "," . $item;
        }
        // Get multile product info
        if ($result = $this->db->query("SELECT id, price FROM $this->dbTable WHERE id IN ($items) ORDER BY id")) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_array()) {
                    $data[] = array(
                        'id' => $row['id'],
                        'price' => $row['price']
                    );
                }
            }
        }
        return $data;
    }

    /**
     * Checks to see if a product exists
     *
     * @param integer $id
     * @return boolean
     */
    public function productExists($id) {
        if($stmt = $this->db->prepare("SELECT id FROM $this->dbTable WHERE id = ?")) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($returnedId);
            $stmt->fetch();
            if($stmt->num_rows > 0) {
                $stmt->close();
                return TRUE;
            } else {
                $stmt->close();
                return FALSE;
            }

        }
    }

    // Creation of page elements

    /**
     * Create product table using info from database
     *
     * @access public
     * @param integer $cols (optional)
     * @param integer $category (optional)
     * @return 
     */
    public function createProductTable($cols=4, $category=NULL) {
        // Get products
        if($category != NULL) {
            // Get products from specific category
            $products = $this->getInCategory($category);
        } else {
            $products = $this->get();
        }
        $data ='';
        // Loop through each product
        if(! empty($products)) {
            $i=1;
            foreach($products as $product) {
                $data .= "<li";
                if($i == $cols) {
                    $data .= " class='last'";
                    $i = 0;
                }
                $data .= "><a href='" . SITE_PATH . "product.php?id={$product['id']}" . "'>";
                $data .= "<img src='" . IMAGE_PATH . $product['image'] . "' alt='" . $product['name'] . "'><br>";
                $data .= "<strong>" . $product['name'] . "</strong></a><br>£" . $product['price'];
                $data .= "<br><a class='buttonSml' href='" . SITE_PATH . "cart.php?id=" . $product['id'] . "'>Add to Cart</a></li>";
                $i++;
            }
        }
        return $data;
    }
}