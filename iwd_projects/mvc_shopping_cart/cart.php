<?php

include("app/init.php");

$template->setData("pageClass", 'shoppingCart');

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    // Check if adding a valid item
    if( ! $products->productExists($_GET['id'])) {
        $template->setAlert("Invalid Item");
        $template->redirect(SITE_PATH . "cart.php");
    }
    // Add item to the cart
    if(isset($_GET['num']) && isnumeric($_GET['num'])) {
        $cart->add($_GET['id'], $_GET['num']);
        $template->setAlert("Items have been added to the cart");
    } else {
        $cart->add($_GET['id']);
        $template->setAlert("Item has been added to the cart");
    }
    // Stops more items being added on refresh
    $template->redirect(SITE_PATH . "cart.php");
    echo "<pre>";
    print_r($_SESSION['cart']);
    echo "</pre>";
} elseif (isset($_GET['id']) && ! is_numeric($_GET['id'])) {
    $template->setAlert("Invalid Item");
    $template->redirect(SITE_PATH . "cart.php");
}

if(isset($_GET['empty'])) {
    $cart->emptyCart();
    $template->setAlert("Shopping cart emptied!");
    $template->redirect(SITE_PATH . "cart.php");
}

if(isset($_POST['update'])) {
    // Get ids of all products in the cart
    $ids = $cart->getIds();
    // Make sure we have ids to work with
    if($ids != NULL) {
        foreach($ids as $id) {
            if(is_numeric($_POST['product' . $id])) {
                $cart->update($id, $_POST['product' . $id]);
            }
        }
        $template->setAlert("Number of items in the cart updated.");
    }
    $template->setData("cartTotalItems", $cart->getTotalItems());
    $template->setData("cartTotalCost", $cart->getTotalCost());
    // unset($_POST['update']);
}

// Get items in cart
$display = $cart->createCart();
$template->setData("cartRows", $display);

// Get category navigation
$categoryNav = $categories->createCategoryNav('');
$template->setData("pageNav", $categoryNav);
$template->load("app/views/v_public_cart.php", 'Shopping Cart!');

// $productsList = $products->getInCategory(2);
// echo "<pre>";
// print_r($productsList);
// echo "</pre>";
// exit;
