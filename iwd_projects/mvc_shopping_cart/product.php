<?php

include('app/init.php');

$template->setData("pageClass", 'product');

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    // Show product
    $product = $products->get($_GET['id']);
    if( ! empty($product)) {
        // Pass product date to the view
        $template->setData('prodId', $_GET['id']);
        $template->setData('prodName', $product['name']);
        $template->setData('prodDescription', $product['description']);
        $template->setData('prodPrice', $product['price']);
        $template->setData('prodImage', IMAGE_PATH . $product['image']);
        // Create category navigation
        $categoryNav = $categories->createCategoryNav($product['categoryName']);
        $template->setData("pageNav", $categoryNav);
        // Display the view
        $template->load("app/views/v_public_product.php", $product['name']);
    } else {
        // Error - Redirect
    $template->redirect(SITE_PATH);
    // $template->setAlert("That product does not exist!", 'error');
    }
} else {
    // Error - Redirect
    $template->redirect(SITE_PATH);
    // $template->setAlert("That product does not exist!", 'error');
}