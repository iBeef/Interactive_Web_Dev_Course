 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>PHP - Chapter 9</title>
 </head>
 <body>
    <h1>PHP Loops</h1>

    <?php
        // For loop
        for($i = 0; $i < 10; $i++) {
            echo "\$i is equal to: $i<br>";
        }

        // While loop
        $x = 0;
        while($x < 10) {
            echo "\$x is equal to: $x<br>";
            $x++;
        }

        $movies = array(
            "Start-Wars", "300", "The Shawshank Redemption",
            "Blade", "Warrior", "Rocky"
        );
        foreach($movies as $movie) {
            echo "I think $movie is a good film!<br>";
        }
    ?>
 </body>
 </html>