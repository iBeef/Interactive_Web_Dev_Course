<?php
    // Start the session
    session_start();

    // Set the session vars
    $_SESSION['faveColour'] = 'blue';
    $_SESSION['faveAnimal'] = 'dog';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 11</title>
</head>
<body>
    <h2>Session variavles created, now let's look at them.</h2>
    <?php
        echo "Favourite colour is {$_SESSION['faveColour']}<br>";
        echo "Favourite animal is {$_SESSION['faveAnimal']}";
    ?>

    <h2>View session with print.</h2>
    <?php
        print_r($_SESSION);
    ?>

    <h2>Reset Session Variables</h2>
    <?php
        $_SESSION['faveColour'] = 'Still blue';
        $_SESSION['faveAnimal'] = 'Still dogs';
        echo "Session variables have been reset.<br>";

        echo "New session variable array:";
        print_r($_SESSION);

    ?>

    <h2>Destroy session variables</h2>
    <?php
        // Remove session variables
        session_unset();
        // Destroy the session
        session_destroy();

        echo "Session destroyed<br>";
        print_r($_SESSION);
    ?>

    <h2>Use a session variable in a conditional statement.</h2>

    <?php
        $_SESSION['faveCar'] = 'Audi';

        // Will remove ['faveCar'] from $_SESSION
        // unset($_SESSION['faveCar']);

        if(isset($_SESSION['faveCar'])) {
            echo "It has been set, it is: {$_SESSION['faveCar']}<br>";
        } else {
            echo "It has not been set!";
            $_SESSION['isAnswered'] = 'No';
        }
    ?>

    <h2>Finally let's check what session is with print_r</h2>
    <?php
        print_r($_SESSION);
    ?>
</body>
</html>