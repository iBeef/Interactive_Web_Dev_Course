<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 11</title>
</head>
<body>
    <?php
        $_SESSION['favColour'] = 'green';
        $_SESSION['favAnimal'] = 'dog';
        echo "Session values are set.";
    ?>
    <h2>PHP Sessions allow you to track people</h2>

    <a href="Ch11_lesson_1_session_info.php">View Session</a>
</body>
</html>