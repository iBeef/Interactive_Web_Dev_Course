<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 11</title>
</head>
<body>
    <h2>PHP Sessions allow you to track people</h2>

    <?php
        echo "Favourite colour is {$_SESSION['favColour']}<br>";
        echo "Favourite animal is {$_SESSION['favAnimal']}";
    ?>

</body>
</html>