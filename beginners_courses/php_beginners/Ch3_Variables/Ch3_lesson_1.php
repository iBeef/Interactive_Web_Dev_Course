<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to PHP - Chapter 3</title>
</head>
<body>
    <h1>PHP Chapter 3 - Variables</h1>

    <?php

    // In php keywords and functions etc. are not case sensitive:

    ECHO "Hellow world!<br>";
    echo "Hellow world!<br>";
    EcHo "Hellow world!<br>";

    // Variables however are case sensitive:

    $dog = 'red';
    echo "My dog is " . $dog . "<br>";
    echo "My cat is " . $DOG . "<br>";
    echo "My tiger is " . $dOg . "<br>";

    ?>
</body>
</html>