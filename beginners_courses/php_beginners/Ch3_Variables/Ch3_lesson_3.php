<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to PHP - Chapter 3</title>
</head>
<body>

    <?php
        $txtS = "Studioweb.com";
        echo "I kinda like $txtS";

        $txtW = "w3schools.com";
        echo "I love" . $txtW . '!';

        echo "Can you add text variables?<br>";
        echo $txtS . $txtW;
        echo "<br>";

        $x = 5;
        $y = 4;

        echo $x + $y . "<br>";

        $beatlesIsCool = TRUE;
        $cultureClubIsCool = FALSE;

        if($cultureClubIsCool) {
            echo "Is it true!";
        } else {
            echo "Sadly it is false";
        }
    ?>

</body>
</html>