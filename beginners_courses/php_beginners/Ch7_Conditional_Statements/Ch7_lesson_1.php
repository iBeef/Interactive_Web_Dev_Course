<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 7</title>
    <style type="text/css">
        input {display: block; margin-bottom: 10px;}
    </style>
</head>
<body>
    <h1>PHP Conditional Statements</h1>

    <form action="Ch7_form_submission.php" method="POST">
        Name: <input type="text" name="name">
        Password: <input type="password" name="password">
        <input type="submit">
    </form>
</body>
</html>