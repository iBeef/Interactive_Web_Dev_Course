<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 7</title>
    <style type="text/css">
        input {display: block; margin-bottom: 10px;}
        select {display: block; margin-bottom: 10px;}
    </style>
</head>
<body>
    <h1>PHP - Switch-Case Statements</h1>

    <form action="Ch7_lesson_3_submission.php" method="POST">
        Name: <input type="text" name="name">
        Favourite Car:
        <select name="favCar">
            <option value="volvo">Volvo</option>
            <option value="saab">Saab</option>
            <option value="mercedes">Mercedes</option>
            <option value="audi">Audi</option>
            <option value="ford">Ford</option>
            <option value="mazda">Mazda</option>
            <option value="crap">Some crappy car brand.</option>
        </select>
        <input type="submit">
    </form>
</body>
</html>