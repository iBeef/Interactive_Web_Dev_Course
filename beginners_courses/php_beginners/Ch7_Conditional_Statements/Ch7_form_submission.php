<?php

    $name = $_POST['name'];
    $password = $_POST['password'];
    $message;

    if($password != 'pass') {
        // Redirect to another site
        header("Location: http://www.google.com");

        // Go back to the previous page.
        // echo "<script> window.history.back() </script>";
        
        // Show a warning.
        // echo "<script> alert('You failed to guess the password') </script>";
        $message = "Sorry, no show for you!";
    } else {
        echo "<script> alert('You got the password') </script>";
        $message = "Welcome to the show!";
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 7</title>
</head>
<body>
    <h1>PHP Conditionals</h1>

    <?php
        echo $message;
    ?>
</body>
</html>