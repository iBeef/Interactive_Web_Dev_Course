<?php

    $name = $_POST['name'];
    $favCar = $_POST['favCar'];

    switch($favCar) {
        case 'audi':
            $message = "Your favourite car is Audi!";
            break;
        case 'mazda':
            $message = "Your favourite car is Mazda!";
            break;
        case 'volvo':
            $message = "Your favourite car is volvo!";
            break;
        default:
            $message = "Couldn't be bothered to list the rest!";
            break;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 7</title>
</head>
<body>
    <h1>PHP - Switch-Case Statements</h1>
    
    <?php
    echo $message;
    ?>
</body>
</html>