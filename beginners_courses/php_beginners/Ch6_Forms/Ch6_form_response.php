<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 6</title>
</head>
<body>
    <h1>PHP Form Processing</h1>
    
    <?php
        // echo "The name entered: {$_POST['name']}";
        // echo "<br>";
        // echo "The password entered: {$_POST['password']}";
        // echo "<br>";
        // echo var_dump($_POST);

        $name = $_POST['name'];
        $password = $_POST['password'];

        if($password != 'pass') {
            echo "No access for you buddy!";
        } else {
            echo "The secret answer is: Crackers";
        }
    ?>
</body>
</html>