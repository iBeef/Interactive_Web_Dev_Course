<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chapter 1 - Getting Started</title>
</head>
<body>
    <h1>Welcome to PHP!</h1>

    <?php
        echo "This is PHP!";
    ?>

    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi vitae corrupti autem sit aut at officia ea non itaque minus?</p>
</body>
</html>