<?php

    function greeting($name = 'some person') {
        echo "Hey there $name!<br>";
    }

    function greeting2($name = 'some person 2') {
        return "Hey there $name!<br>";
    }

    function greeting3($firstName, $lastName) {
        return "Hey there buddy, your name is $firstName $lastName!<br>";
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 10</title>
</head>
<body>
    <h1>Chapter 10 - Custom Functions</h1>

    <?php echo greeting2('John'); ?>
    <?php echo greeting2('Jamie'); ?>
    <?php echo greeting2('Ser Davos'); ?>
    <?php echo greeting2(); ?>
    <?php greeting(); ?>
    <?php echo greeting3('John', 'Snow'); ?>
    <?php echo greeting3('Jamie', 'Lannister'); ?>

</body>
</html>