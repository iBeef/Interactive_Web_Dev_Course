<style type="text/css">
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }

    li {
        float: left;
        margin-left: 0px;
    }

    a {
        display: block;
        width: 80px;
        background-color: #DDDDDD;
        text-align: center;
        padding: 4px;
        text-decoration: none;
        text-transform: uppercase;
    }

    a:hover {
        background-color: #DDFF00;
    }

</style>

<h1>Css Navbars!</h1>

<ul>
    <li><a href="#home">Home</a></li>
    <li><a href="#news">News</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#newNav">New Nav</a></li>
    <li><a href="#about">About</a></li>
</ul>

<?php 
    $password = "bigFish10";
?>