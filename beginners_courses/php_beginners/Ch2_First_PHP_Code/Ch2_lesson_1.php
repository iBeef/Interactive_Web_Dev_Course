<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chapter 2 - PHP</title>
</head>
<body>
    <h1>Welcome to PHP</h1>

    <?php
        echo "This is some PHP";
    ?>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit dolore iste nulla fugiat, eius sit maiores hic ut alias quae incidunt, quibusdam aliquam eos. Voluptates ea vitae fugiat cumque, molestiae, quod labore quis reprehenderit explicabo neque repellat autem laudantium beatae tempore exercitationem veritatis ratione ab ducimus? Dignissimos explicabo iste soluta!</p>
</body>
</html>