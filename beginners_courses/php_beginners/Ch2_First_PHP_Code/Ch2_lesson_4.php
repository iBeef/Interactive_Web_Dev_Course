<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chapter 2 - PHP</title>
</head>
<body>
    <h1>Welcome to PHP - Chapter 2</h1>

    <?php
        // This is a single line comment

        # This is another single line comment

        /* This
        is a 
        multi-line comment
        */

        // You can also use comments to remove parts from a line of code:
        $x = 5 /* + 15 */ + 5;
        $secretVar = 'password';
        echo $x;
    ?>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, nihil ab. Cupiditate sapiente natus magnam dolorem explicabo molestiae, incidunt perferendis soluta in nisi consectetur, numquam ipsum placeat veritatis asperiores a hic! Nemo illum sed consectetur sint vel. Perferendis id cum, quidem debitis similique voluptatem odit dolores labore ad voluptas quaerat.</p>
</body>
</html>