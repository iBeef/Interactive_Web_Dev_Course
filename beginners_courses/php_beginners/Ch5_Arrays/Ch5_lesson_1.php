<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chapter 5 - Arrays</title>
</head>
<body>
    <h1>PHP Arrays</h1>

    <?php

        /*
        // Indexed Arrays

        $foodCart = array('bananas', 'burgers', 'chicken', 'butter');

        // echo "What's in the array: $foodCart[2]";

        echo "How many items in our cart: " . count($foodCart) . "<br>";

        for($i = 0; $i < count($foodCart); $i++) {
            echo "The item at position {$i} is: {$foodCart[$i]}<br>";
        }
        */

        // Associative Arrays
        $assocArray = array('Stefan' => '44', 'Nick' => '32', 'Mary' => '27');

        echo "Stefan's age is {$assocArray['Stefan']}<br>";

        echo "Count: " . count($assocArray);

    ?>

</body>
</html>