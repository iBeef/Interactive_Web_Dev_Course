<?php

// Start the session
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP - Chapter 12</title>
    <link href="https://fonts.googleapis.com/css?family=Calligraffitti" rel="stylesheet">
    <link rel="stylesheet" href="ch12_styles.css">
</head>
<body>
    <nav>
        <ul>
            <li><a href="ch12_login.php">Home</a></li>
            <li><a href="ch12_protected_page.php">Protected Page</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="ch12_logout.php">Logout</a></li>
        </ul>
    </nav>

    <main>

        <?php
            if(isset($_SESSION["isLoggedIn"])) {
                echo "<p>You're already logged in buddy!</p>";
            } else {
// Unindented to keep editor happy 
$theForm = <<<THEFORM
    <p>Welcome to WheatBook!</p>
    <h2>Please enter your username and password to log in.</h2>

    <form method="POST" action="ch12_login_response.php">
        <input type="text" name="username" id="username">
        <input type="password" name="password">
        <input type="submit">
    </form>
THEFORM;
// Herdoc end
                echo $theForm;
            }
        ?>

        <?php
            // Using the query string to send messages back to the login page.
            // $isBlock = $_GET["isBlock"];
            // $badUserCredentials = $_GET["badUserCredentials"];
            // $isBlock = (isset($_GET["isBlock"])) ? $_GET["isBlock"] : null;
            // $badUserCredentials = (isset($_GET["badUserCredentials"])) ? $_GET["badUserCredentials"] : null;
            $isBlock = $_GET["isBlock"] ?? null;    // Checks if exists if it doesnt returns null;
            $badUserCredentials = $_GET["badUserCredentials"] ?? null;

            if(isset($isBlock)) {
                echo "<h2>Ah ha! You need to log in buddy.</h2>";
                echo "<script>document.getElementById('username').focus()</script>";
            } elseif($badUserCredentials) {
                echo "<h2>Username or password is wrong buddy!</h2>";
                echo "<script>document.getElementById('username').focus()</script>";
            }
        ?>

    </main>
</body>
</html>