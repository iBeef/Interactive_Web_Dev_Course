<?php
    #Start the session
    session_start();

    if(isset($_SESSION["isLoggedIn"])) {
        // pass
    } else {
        header("Location: ch12_login.php?isBlock=true");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ch12 - Protected Page</title>
    <link href="https://fonts.googleapis.com/css?family=Calligraffitti" rel="stylesheet">
    <link rel="stylesheet" href="ch12_styles.css">
</head>
<body>
    <nav>
        <ul>
            <li><a href="ch12_login.php">Home</a></li>
            <li><a href="ch12_protected_page.php">Protected Page</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="ch12_logout.php">Logout</a></li>
        </ul>
    </nav>
    <main>
        <p>You're now viewing the members only VIP section!!!!!!</p>
    </main>
</body>
</html>
