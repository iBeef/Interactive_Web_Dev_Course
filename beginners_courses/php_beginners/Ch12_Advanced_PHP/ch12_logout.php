<?php
// Start the session
session_start();

// Remove all the session variables
session_unset();

// Destroy the session
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ch12 - Protected Page</title>
    <link href="https://fonts.googleapis.com/css?family=Calligraffitti" rel="stylesheet">
    <link rel="stylesheet" href="ch12_styles.css">
</head>
<body>
    <nav>
        <ul>
            <li><a href="ch12_login.php">Home</a></li>
            <li><a href="ch12_protected_page.php">Protected Page</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="ch12_logout.php">Logout</a></li>
        </ul>
    </nav>
    <main>
        <p>You're now logged out.</p>
    </main>
</body>
</html>

