<?php
    // Start the session
    session_start();

    // Check if the correct username was used, not bothering with password for the time being.

    $username = $_POST['username'];

    if(trim($username) === 'Jimmy') {
        $_SESSION['isLoggedIn'] = true;
        header("Location: ch12_protected_page.php");
    } else {
        header("Location: ch12_login.php?badUserCredentials=true");
    }
?>