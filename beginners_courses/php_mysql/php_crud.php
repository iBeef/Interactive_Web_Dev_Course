<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MySQLi vs PHP-PDO</title>
</head>
<body>
    <h1>Connecting Databases with PHP</h1>
    <h2>MySQLi vs PHP-PDO</h2>

    <?php
        // Set connection vars
        $serverName = "localhost";
        $username = "killer_lewis";
        $password = "password123";
        $dbname = "killer";
        
        // Create a connection
        $conn = new MySQLi($serverName, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection Error: {$conn->connect_error}");
        } else {
            echo "Connection success<br>";
        }
        
        // Fetch data
        $sql = "SELECT * FROM products";
        $sql = "SELECT * FROM products WHERE product_id = 1254";
        $result = $conn->query($sql);
        if($result->num_rows > 0) {
            // Fetch data
            while($row = $result->fetch_assoc()) {
                echo "id: {$row['id']} - Product id: {$row['product_id']} - Product Name: {$row['product_name']} - Product Price: {$row['product_price']}<br>";
            }
        } else {
            echo "0 results found.";
        }
        
        $conn->close();
    ?>

</body>
</html>