function startEdit() {
    var element = document.querySelector("#editableElement");
    element.contentEditable = true;
    element.focus();
}

function stopEdit() {
    var element = document.querySelector("#editableElement");
    element.contentEditable = false;
    
    // Show written content.
    alert("This is what you wrote:\n" + element.innerHTML);
}

document.querySelector("#startEdit").onclick = function() {
    startEdit();
}

document.querySelector("#stopEdit").onclick = function() {
    stopEdit();
}