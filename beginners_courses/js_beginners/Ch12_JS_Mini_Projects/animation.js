var notRunning = true;

function getRandomColor() {
    var letters = "0123456789ABCDEF".split('');
    var color = '#';
    for(var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function discoSquare() {
    var canvas = document.querySelector("#whiteBoard");
    var context = canvas.getContext('2d');

    var vertPos = Math.floor((Math.random() * 399) + 1);
    var theColor = getRandomColor();

    context.lineWidth = 10;
    context.strokeStyle = theColor;
    context.moveTo(10, vertPos);
    console.log(`Vertical Position: ${vertPos}`);
    context.lineTo(400, vertPos);
    context.lineCap = 'butt';
    context.stroke();

    document.querySelector("#displayColor").innerHTML = theColor;
}

var animation;

document.querySelector("#discoStart").onclick = function() {
    if(notRunning) {
        animation = setInterval(discoSquare, 100);
        notRunning = false;
    } else {
        alert("Sorry already running!");
    }
}

document.querySelector("#discoStop").onclick = function() {
    clearInterval(animation);
    notRunning = true;
}