document.querySelector("#whiteBoard").onclick = function () {
    var canvas = document.querySelector('canvas');
    var context = canvas.getContext('2d');

    // Set the line width and colour.
    context.lineWidth = 20;
    context.strokeStyle = "rgb(205, 40, 40)";

    // Draw the first line
    context.moveTo(25, 50);
    context.lineTo(400, 120);
    context.lineCap = 'butt';
    context.stroke();

    // Draw a second line with different colour and line width.
    context.lineWidth = 15;
    context.strokeStyle = "#95CA91";

    // context.beginPath() tells context to start a new line.
    context.beginPath();
    context.moveTo(25, 120);
    context.lineTo(400, 120);
    context.lineCap = 'butt';
    context.stroke();
}