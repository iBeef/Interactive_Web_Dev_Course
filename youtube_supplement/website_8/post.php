<?php
    // Import db connection
    require("config/config.php");
    require("config/db.php");

    if(isset($_POST['delete'])) {
        echo "Is submitted";
        // Get form data
        $delete_id = mysqli_real_escape_string($conn, $_POST['delete_id']);

        $query = "DELETE FROM posts WHERE id = $delete_id";

        if(mysqli_query($conn, $query)) {
            header("Location:" . ROOT_URL);
        } else {
            echo "Error: " . mysqli_error($conn);
        }
    }

    // Get id
    $id = mysqli_real_escape_string($conn, $_GET['id']);

    // Create db query
    $query = "SELECT * FROM posts WHERE id = $id";
    $result = mysqli_query($conn, $query);

    // Fetch data
    $post = mysqli_fetch_assoc($result);
    // var_dump($posts);

    // Free result
    mysqli_free_result($result);

    // Close connection
    mysqli_close($conn);
?>

<?php include("inc/header.php"); ?>
    <div class="container mt-5 pt-4">
        <h1><?php echo $post['title']; ?></h1>
        <small>Created on <?php echo $post["created_at"]; ?> by <?php echo $post['author']; ?></small>
        <p><?php echo $post['body'] ?></p>
        <hr>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" class="d-inline">
            <input type="hidden" name="delete_id" value="<?php echo $post['id'] ?>">
            <input type="submit" name="delete" value="Delete" class="btn btn-warning btn-sm">
        </form>
        <a href="<?php echo ROOT_URL ?>editpost.php?id=<?php echo $post['id'] ?>" class="btn btn-success btn-sm ml-1">Edit</a>
        <a href="<?php echo ROOT_URL ?>" class="btn btn-primary btn-sm  ml-1">Back</a>
    </div>
<?php include("inc/footer.php"); ?>
