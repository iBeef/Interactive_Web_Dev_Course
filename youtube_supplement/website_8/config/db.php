<?php
    // Create connection
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    // Check the connection
    if(mysqli_connect_errno()) {
        // Connection failed
        echo "Failed to connect to mysql" . mysqli_connect_errno();
    }
?>