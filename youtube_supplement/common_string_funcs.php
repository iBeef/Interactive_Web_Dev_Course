<?php

    // substr() - Returns a portion of a string.
    // $output = substr('Hello', 1, 3);
    // $output = substr('Hello', -2);
    // echo $output;

    // strlen() - Returns the length of a string.
    // $output = strlen("Hello World!");
    // echo $output;

    // strpos() - Finds the first occurence of a substring.
    // $output = strpos("he did did not!", 'did');
    // echo $output;

    // strrpos() - Finds the last occurence of a substring.
    // $output = strrpos("he did did not!", 'did');
    // echo $output;

    // trim() - Trims white space.
    // $text = "Hello world!          ";
    // echo strlen($text) . '<br>';
    // echo strlen(trim($text));

    // strtoupper()/strtolower() - Sets string to upper/lower case.
    // $output = strtoupper("hello world");
    // echo $output;

    // ucwords() - Capitalises every word.
    // $output = ucwords("my silly string");
    // echo $output;

    // str_replace() - Replaces all ocurences of a search string with a replacement.
    // $text = "hello world";
    // $output = str_replace('world', 'universe', $text);
    // echo $output;

    // is_string() - Test to see if something is a string or not.
    // echo is_string('string') . "<br>";
    // echo is_string(5);

    // $values = array(TRUE, FALSE, NULL, 'abc', 33, '33', 22.4, '22.4', '', ' ', 0, '0');
    // foreach($values as $value) {
    //     if(is_string($value)) {
    //         echo "$value is a string!<br>";
    //     } else {
    //         echo "$value is not a string!<br>";
    //     }
    // }

    // gzcompress()/gzuncompress() - Compress/uncompress a string.
    $string = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Autem sapiente quam iusto possimus quia laudantium, odio saepe consequatur? Facilis porro quod voluptatibus minima quisquam sequi rerum doloremque numquam natus, placeat nobis aliquid asperiores architecto iusto maxime aut laudantium voluptatum amet, eaque ab suscipit est? Pariatur quos fuga officia architecto quis?";

    $compressed = gzcompress($string);
    echo $compressed;
    
?>