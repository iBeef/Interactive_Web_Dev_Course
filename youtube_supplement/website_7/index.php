<?php
    $path = "/dir0/dir1/myFile.php";
    $file = "file1.txt";
    
    // Return filename
    # echo basename($path);

    // Return filename without extensiion
    # echo basename($path, '.php');

    // Return the dir name from the path
    # echo dirname($path);

    // Check if file exists, also works for folders
    # echo file_exists($file) ? "Files exists!" : "Files does not exist";

    // Get abs path
    # echo realpath($file);

    // Check to see if file, does not work for folders.
    # echo is_file($file);

    // Check if a file is writable
    # echo is_writeable($file);

    // Check if a file is readble
    # echo is_readable($file);

    // Get file size
    # echo filesize($file);

    // Create a directory
    # mkdir('testing');

    // Delete a directory
    # rmdir('testing');

    // Copy a file
    # echo copy($file, "file2.txt");

    // Rename a file
    # rename("file2.txt", "myFile.txt");

    // Delete a file
    # unlink("myFile.txt");

    // Write from file to string
    # echo file_get_contents($file);

    // Write string to the file
    # echo file_put_contents($file, "Goodbye World");

    // Write to file and preserve current data.
    # $current = file_get_contents($file);
    # $current .= " Goodbye World";
    # file_put_contents($file, $current);

    // Open file for reading
    # $handle = fopen($file, 'r');
    # $data = fread($handle, filesize($file));
    # echo $data;
    # fclose($handle);

    // Open file for writing
    $handle = fopen("file2.txt", 'w');
    $txt = "John Doe\n";
    fwrite($handle, $txt);
    $txt = "Steve Smith";
    fwrite($handle, $txt);
    fclose($handle);

?>