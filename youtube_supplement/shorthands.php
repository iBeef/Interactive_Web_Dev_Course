<?php

    // $loggedIn = TRUE;
    $loggedIn = FALSE;

    $listValues = [
        'Home', 'About',
        'News', 'Contact' 
    ]

    // if($loggedIn) {
    //     echo "You are logged in!";
    // } else {
    //     echo "You are not logged in!";
    // }

    // Short hand if statement:
    // echo ($loggedIn) ? "You are logged in!" : "You are not logged in!";
    
    // $isRegistered = ($loggedIn) ? TRUE : FALSE;
    // echo $isRegistered;

    // $age =11;
    // $score = 20;

    // // Nested shorthand if statements
    // echo "Your score is " . ($score > 10 ? ($age > 10 ? 'average' : 'execptional') :
    //     ($age > 10 ? 'horrible' : 'average'));
?>

<!-- Normal if statement in html -->
<div>
    <?php if($loggedIn) { ?>
        <h1>Welcome User</h1>
    <?php } else { ?>
        <h1>Welcome Guest</h1>
    <?php } ?>
</div>
<!-- shorthand for loops etc. when embedding in html -->
<div>
    <?php if($loggedIn): ?>
        <h1>Welcome User</h1>
    <?php else: ?>
        <h1>Welcome Guest</h1>
    <?php endif; ?>
</div>

<!-- Not limited to if statements -->
<ul>
    <?php foreach($listValues as $value): ?>
        <li><?php echo $value; ?></li>
    <?php endforeach; ?>
</ul>