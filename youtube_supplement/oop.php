<?php
    class Person{
        private $name;
        private $email;
        // public static $ageLimit = 40;
        private static $ageLimit = 40;

        // Class constructor
        public function __construct($name, $email) {
            $this->name = $name;
            $this->email = $email;
            echo __CLASS__ . " created<br>";
        }

        // Class deconstructor
        public function __destruct() {
            echo __CLASS__ . " destroyed!<br>";
        }

        public function setName($name) {
            $this->name = $name;
        }

        public function getName() {
            return $this->name . "<br>";
        }

        public function setEmail($email) {
            $this->email = $email;
        }

        public function getEmail() {
            return $this->email . "<br>";
        }

        public static function getAgeLimit() {
            return self::$ageLimit;
        }
    }

    // Instantiate the class
    // $person1 = new Person("John Doe", "john@test.com");

    // Ways to access the class variables directly.
    // $person1->name = "John Doe";
    // echo $person1->name;

    // Using setters and getters.
    // $person1->setName("John Doe");
    // echo $person1->getName();

    // Fetch the static property and method
    // echo Person::$ageLimit;
    echo Person::getAgeLimit();

    // Class Inheritence 
    class Customer extends Person {
        private $balance;

        public function __construct($name, $email, $balance) {
            parent::__construct($name, $email, $balance);
            $this->balance = $balance;
            echo "A new " . __CLASS__ . " has been created.<br>";
        }

        public function setBalance($balance) {
            $this->balance = $balance;
        }

        public function getBalance() {
            return $this->balance . "<br>";
        }

    }

    // $customer1 = new Customer("John Doe", "john@test.com", 300);

    // echo $customer1->getBalance();
?>