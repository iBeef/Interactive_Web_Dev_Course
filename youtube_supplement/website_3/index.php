<?php
    // Message vars
    $msg = '';
    $msg_Class = '';
    // Check fro submit
    if(filter_has_var(INPUT_POST, 'submit')) {
        // echo "Submitted!";
        // Get data
        $name = htmlspecialchars($_POST['name']);
        $email = htmlspecialchars($_POST['email']);
        $message = htmlspecialchars($_POST['message']);

        // Check required fields
        if(!empty($email) && !empty($name) && !empty($message)) {
            if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
                $msg = "Invalid email address";
                $msgClass = "alert-danger";
            } else {
                // Send an email
                $toEmail = "test@test.com";
                $subject = "Contact Request From $name";
                $body = "<h2>Contact Request</h2>
                    <h4>Name</h4>$name<p></p>
                    <h4>Email</h4>$email<p></p>
                    <h4>Message</h4><p>$message</p>
                ";
                // Email Headers
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-Type:text/html;charset=UTF-8" . "\r\n";
                // Additional Headers
                $headers .= "From: $name<$email>" . "\r\n";

                if(mail($toEmail, $subject, $body, $headers)) {
                    // Email Sent
                    $msg = "Your email has been sent.";
                    $msgClass = "alert-success";
                } else {
                    // Email Failed to send
                    $msg = "Your email was not sent";
                    $msgClass = "alert-danger";
                }

            }
        } else {
            $msg = "Please fill in all fields";
            $msgClass = "alert-danger";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Us</title>
    <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">My Website</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <?php if($msg != ''): ?>
            <div class="alert <?php echo $msgClass; ?>"><?php echo $msg; ?></div>
        <?php endif; ?>
        <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
            <div class="form-group">
                <label>Name</label>
                <!-- <input type="text" name="name" class="form-control" value="<?php echo isset($_POST['name']) ? $name : '' ?>"> -->
                <input type="text" name="name" class="form-control" value="<?php echo $_POST['name'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>email</label>
                <input type="text" name="email" class="form-control" value="<?php echo $_POST['email'] ?? '' ?>">
            </div>
            <div class="form-group">
                <label>Message</label>
                <textarea type="text" name="message" class="form-control"><?php echo $_POST['message'] ?? '' ?></textarea>
            </div>
            <br>
            <input type="submit" name="submit" class="btn btn-primary">
        </form>
    </div>
</body>
</html>