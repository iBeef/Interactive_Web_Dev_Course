<?php
    // GET method
    if(isset($_GET['name'])) {
        $name = htmlentities($_GET['name']);
        // $email = htmlentities($_GET['email']);
        echo "$name<br>";
        // echo $email;
        // print_r($_GET);
    }
    /*
    // POST Method
    if(isset($_POST['name'])) {
        $name = htmlentities($_POST['name']);
        $email = htmlentities($_POST['email']);
        echo "$name<br>";
        echo $email;
        // print_r($_POST);
    }

    // REQUEST Method
    if(isset($_REQUEST['name'])) {
        $name = htmlentities($_REQUEST['name']);
        $email = htmlentities($_REQUEST['email']);
        echo "$name<br>";
        echo $email;
        // print_r($_REQUEST);
    } */

    echo $_SERVER['QUERY_STRING'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>POST Form</title>
</head>
<body>
    <form action="get_post.php" method="GET">
    <!-- <form action="get_post.php" method="POST"> -->
        <div>
            <label>Name:</label><br>
            <input type="text" name="name">
        </div>
        <div>
            <label>Email:</label><br>
            <input type="text" name="email">
        </div>
        <input style="margin-top: 5px;" type="submit">
    </form>
    <!-- You can also submit a get request manually -->
    <ul>
    <li>
        <a href="get_post.php?name=Lewis">Get Me</a>
    </li>
    <li>
        <a href="get_post.php?name=Steve">Get Steve</a>
    </li>
    </ul>
    <h1><?php echo $name ?>'s profile</h1>
</body>
</html>