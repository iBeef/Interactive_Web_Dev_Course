<?php
    /*
    // if(filter_has_var(INPUT_POST, 'data')) {
    //     echo 'Data found';
    // } else {
    //     echo 'Data not found';
    // }
    */

    /*
    if(filter_has_var(INPUT_POST, 'data')) {
        $email = $_POST['data'];
        echo $email . "<br>";
        // Remove illegal characters
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        echo $email . "<br>";
    */

        /*
        For input:
        if(filter_input(INPUT_POST, 'data', FILTER_VALIDATE_EMAIL)) {
            echo "Email is valid";
        } else {
            echo "Email is not valid";
        }
        */

        /*
        // For var for example if it needs to be sanitized first:
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "Email is valid";
        } else {
            echo "Email is not valid";
        }
        */

    // }

    //  A list of all the validation types:
    # FILTER_VALIDATE_BOOLEAN
    # FILTER_VALIDATE_EMAIL
    # FILTER_VALIDATE_FLOAT
    # FILTER_VALIDATE_INT
    # FILTER_VALIDATE_IP
    # FILTER_VALIDATE_REGEXP
    # FILTER_VALIDATE_URL

    // A list of the sanitize types
    # FILTER_SANITIZE_EMAIL
    # FILTER_SANITIZE_ENCODED
    # FILTER_SANITIZE_NUMBER_FLOAT
    # FILTER_SANITIZE_NUMBER_INT
    # FILTER_SANITIZE_SPECIAL_CHARS
    # FILTER_SANITIZE_STRING
    # FILTER_SANITIZE_URL

    /*
    // Validate int
    $var = 23;              // Returns true
    $var = '23';            // Returns true id just numbers in string
    $var = "Hello World";   // Returns FALSE
    if(filter_var($var, FILTER_VALIDATE_INT)) {
        echo "$var is a number<br>";
    } else {
        echo "$var is not a number<br>";
    }
    */

    /*
    // $var = "ndndns3783447nsdnsdn33nerd3";
    $var = "<script>alert('hacked');</script>";

    // var_dump(filter_var($var, FILTER_SANITIZE_NUMBER_INT));  // Sanitize number string
    echo filter_var($var, FILTER_SANITIZE_SPECIAL_CHARS);     // Sanitize special characters
    */

    /*
    // Filter input array
    $filters = array(
        'data' => FILTER_VALIDATE_EMAIL,
        'data2' => array(
            'filter' => FILTER_VALIDATE_INT,
            'options' => array(
                "min_range" => 1,
                "max_range" => 100
            )
        )
    );

    print_r(filter_input_array(INPUT_POST, $filters));
    */

    // Filter var array
    $arr = array(
        'name' => "john smith",
        'age' => 23,
        'email' => "test@test.com"
    );

    $filters = array(
        'name' => array(
            'filter' => FILTER_CALLBACK,
            'options' => "ucwords"
        ),
        'age' => array(
            'filter' => FILTER_VALIDATE_INT,
            'options' => array(
                "min_range" => 1,
                "max_range" => 120
            )
        ),
        'email' => FILTER_VALIDATE_EMAIL
    );

    print_r(filter_var_array($arr, $filters));

?>
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
    <input type="text" name="data">
    <input type="text" name="data2">
    <button>Submit</button>
</form>