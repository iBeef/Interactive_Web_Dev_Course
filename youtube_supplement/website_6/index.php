<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
    <script>
        function showSuggestion(str) {
            if(str.length === 0) {
                document.querySelector("#output").innerHTML = '';
            } else {
                // AJAX Request
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.onreadystatechange = function () {
                    if(this.readyState === 4 && this.status == 200) {
                        document.querySelector("#output").innerHTML = this.responseText;
                    }
                }
                xmlHttp.open('GET', `suggest.php?q=${str}`, true);
                xmlHttp.send();

            }
        }
    </script>
</head>
<body>
    <div class="container">
        <H1>Search Users</H1>
        <form action="">
            Search Users: <input type="text" name="" class="form-control" onkeyup="showSuggestion(this.value)">
        </form>
        <p>Suggestions: <span id="output" style="font-weight:bold"></span></p>
    </div>
</body>
</html>